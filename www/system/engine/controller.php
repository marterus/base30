<?php
/**
 * @package		OpenCart
 * @author		Daniel Kerr
 * @copyright	Copyright (c) 2005 - 2017, OpenCart, Ltd. (https://www.opencart.com/)
 * @license		https://opensource.org/licenses/GPL-3.0
 * @link		https://www.opencart.com
*/

/**
* Controller class
*/
/**
 * =========================================================================================
 * Define your models either here or in the actual controllers using them
 * *******************************************************
 * Mark system/engine/controller.php as Plain Text
 * Mark system/engine/model.php as Plain Text
 * Mark system/engine/loader.php as Plain Text
 * *******************************************************
 * The basic idea is we are telling the IDE what var or property belongs to what class
 * The result gives use code completion and suggestion
 *
 * If you have this file in the project and open the ControllerCatalogProduct class you will
 * see everything is loading correctly in the IDE and code completion works for both core and
 * models for this module
 * =========================================================================================
 *
 * -----------------------------------------------------
 * EXAMPLE - OpenCart's Catalog Product Controller Models
 * Class ControllerCatalogProduct
 * -----------------------------------------------------
 * @property ModelSaleCustomerGroup       $model_sale_customer_group
 * @property ModelCatalogProduct          $model_catalog_product
 * @property ModelToolImage               $model_tool_image
 * @property ModelToolMicrodata           $model_tool_microdata
 * @property ModelLocalisationLanguage    $model_localisation_language
 * @property ModelSettingStore            $model_setting_store
 * @property ModelCatalogOption           $model_catalog_option
 * @property ModelLocalisationTaxClass    $model_localisation_tax_class
 * @property ModelLocalisationStockStatus $model_localisation_stock_status
 * @property ModelLocalisationCountry     $model_localisation_country
 * @property ModelLocalisationWeightClass $model_localisation_weight_class
 * @property ModelLocalisationLengthClass $model_localisation_length_class
 * @property ModelCatalogManufacturer     $model_catalog_manufacturer
 * @property ModelCatalogFilter           $model_catalog_filter
 * @property ModelCatalogFilterPattern    $model_catalog_filter_pattern
 * @property ModelCatalogAttribute        $model_catalog_attribute
 * @property ModelCatalogDownload         $model_catalog_download
 * @property ModelBlogPost                $model_blog_post
 * @property ModelCustomerCustomer        $model_customer_customer
 * @property ModelBlogCategory            $model_blog_category
 * @property ModelBlogSetting             $model_blog_setting
 * @property ModelCatalogSeries			  $model_catalog_series
 * @property ModelCheckoutNeoSeoDroppedCart			  $model_checkout_neoseo_dropped_cart
 * @property ModelCatalogZfilter		  $model_catalog_zfilter
 * @property ModelModuleNeoseoProductBundles $model_module_neoseo_product_bundles
 * @property ModelSaleSoforpOrderManager $model_sale_soforp_order_manager
 * @property ModelSimpleBlogArticle $model_simple_blog_article
 * @property ModelCatalogWarehouse $model_catalog_warehouse
 *
 *
 * -----------------------------------------------------
 * EXAMPLE - A couple of OpenCart's Catalog Category Controller Models
 * -----------------------------------------------------
 * @property ModelCatalogCategory         $model_catalog_category
 * @property ModelSettingSetting          $model_setting_setting
 * @property ModelDesignLayout            $model_design_layout
 *
 * =========================================================================================
 * =========================================================================================
 * =========================================================================================
 * =========================================================================================
 * -----------------------------------------------------
 * OpenCart Library Classes
 * -----------------------------------------------------
 * @property Affiliate                    $affiliate
 * @property Cache                        $cache
 * @property Captcha                      $captcha
 * @property Cart                         $cart
 * @property Config                       $config
 * @property Currency                     $currency
 * @property Customer                     $customer
 * @property MySQL                        $db
 * @property Document                     $document
 * @property Encryption                   $encryption
 * @property Loader                       $load
 * @property Image                        $image
 * @property Language                     $language
 * @property Length                       $length
 * @property Log                          $log
 * @property Mail                         $mail
 * @property Pagination                   $pagination
 * @property Request                      $request
 * @property Response                     $response
 * @property Session                      $session
 * @property Tax                          $tax
 * @property Template                     $template
 * @property Url                          $url
 * @property User                         $user
 * @property Weight                       $weight
 * @property RedisConnect                 $redis
 *
 * -----------------------------------------------------
 * The actual Controller Class properties and methods
 * -----------------------------------------------------
 * @property                              $registry
 * @property                              $id
 * @property                              $layout
 * @property                              $children
 * @property                              $data
 * @property                              $output
 *
 * @method forward
 * @method redirect
 * @method getChild
 * @method render
 */

abstract class Controller {
	protected $registry;

	public function __construct($registry) {
		$this->registry = $registry;
	}

	public function __get($key) {
		return $this->registry->get($key);
	}

	public function __set($key, $value) {
		$this->registry->set($key, $value);
	}
}