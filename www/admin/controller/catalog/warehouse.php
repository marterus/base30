<?php
class ControllerCatalogWarehouse extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/warehouse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/warehouse');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/warehouse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/warehouse');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_warehouse->addWarehouse($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function save() {
		$this->load->language('catalog/warehouse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/warehouse');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->load->model('setting/setting');

			$this->model_setting_setting->editSetting('config_warehouse', ['config_warehouse_option' => $this->request->post['warehouse']]);
			$this->model_catalog_warehouse->dropWarehouse();
		}

		$this->getList();
	}

	public function edit() {
		$this->load->language('catalog/warehouse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/warehouse');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_warehouse->editWarehouse($this->request->get['warehouse_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/warehouse');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/warehouse');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $warehouse_id) {
				$this->model_catalog_warehouse->deleteWarehouse($warehouse_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'fgd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = (int)$this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/warehouse/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/warehouse/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);
		$data['save'] = $this->url->link('catalog/warehouse/save', 'user_token=' . $this->session->data['user_token'] . $url, true);

		$data['warehouses'] = array();

		$this->load->model('catalog/option');
		$this->load->model('setting/setting');

		$config_warehouse = $this->model_setting_setting->getSetting('config_warehouse');

		$data['option_for_warehouse'] = $config_warehouse ? $config_warehouse['config_warehouse_option'] : null;

		$data['options'] = [];
		$options = $this->model_catalog_option->getOptions();
		foreach ($options as $option) {
			$data['options'][] = [
				'option_id' => $option['option_id'],
				'name' => $option['name'],
			];
		}

		if ($config_warehouse) {
			$option_values = $this->model_catalog_option->getOptionValues($data['option_for_warehouse']);
			foreach ($option_values as $option) {
				$data['warehouses'][] = [
					'warehouses_id' => $option['option_value_id'],
					'name' => $option['name'],
					'edit' => $this->url->link('catalog/warehouse/edit', 'user_token=' . $this->session->data['user_token'] . '&warehouse_id=' . $option['option_value_id'], true),
				];
			}
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . '&sort=fgd.name' . $url, true);
		$data['sort_sort_order'] = $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . '&sort=fg.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/warehouse_list', $data));
	}

	protected function getForm() {
		$data['text_form'] = !isset($this->request->get['warehouse_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['group'])) {
			$data['error_group'] = $this->error['group'];
		} else {
			$data['error_group'] = array();
		}

		if (isset($this->error['warehouse'])) {
			$data['error_warehouse'] = $this->error['warehouse'];
		} else {
			$data['error_warehouse'] = array();
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		if (!isset($this->request->get['warehouse_id'])) {
			$data['action'] = $this->url->link('catalog/warehouse/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/warehouse/edit', 'user_token=' . $this->session->data['user_token'] . '&warehouse_id=' . $this->request->get['warehouse_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/warehouse', 'user_token=' . $this->session->data['user_token'] . $url, true);


		$this->load->model('catalog/option');
		$this->load->model('setting/setting');
		$this->load->model('localisation/country');

		$config_warehouse = $this->model_setting_setting->getSetting('config_warehouse');

		$data['option_for_warehouse'] = $config_warehouse['config_warehouse_option'];

		$option_values = $this->model_catalog_option->getOptionValue($this->request->get['warehouse_id']);

		$data['name'] = $option_values['name'];

		$countries = $this->model_localisation_country->getCountries();
		foreach ($countries as $country) {
			$data['countries'][] = [
				'country_id' => $country['country_id'],
				'name' => $country['name'],
			];
		}
		$data['json_countries'] = json_encode($data['countries']);

		$data['user_token'] = $this->session->data['user_token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();


		if (isset($this->request->post['warehouse'])) {
			$data['warehouses'] = $this->request->post['warehouse'];
		} elseif (isset($this->request->get['warehouse_id'])) {
			$data['warehouses'] = $this->model_catalog_warehouse->getWarehouseCountries($this->request->get['warehouse_id']);
		} else {
			$data['warehouses'] = array();
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/warehouse_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/warehouse')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/warehouse')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['warehouse_name'])) {
			$this->load->model('catalog/warehouse');

			$warehouse_data = array(
				'warehouse_name' => $this->request->get['warehouse_name'],
				'start'       => 0,
				'limit'       => 5
			);

			$warehouses = $this->model_catalog_warehouse->getWarehouses($warehouse_data);

			foreach ($warehouses as $warehouse) {
				$json[] = array(
					'warehouse_id' => $warehouse['warehouse_id'],
					'name'      => strip_tags(html_entity_decode($warehouse['group'] . ' &gt; ' . $warehouse['name'], ENT_QUOTES, 'UTF-8'))
				);
			}
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}