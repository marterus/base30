<?php
class ModelCatalogWarehouse extends Model {
	public function dropWarehouse() {
		$this->db->query("TRUNCATE `" . DB_PREFIX . "warehouse_to_region`");
	}

	public function editWarehouse($warehouse_id, $data) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "warehouse_to_region` WHERE warehouse_id = '" . (int)$warehouse_id . "'");
		foreach ($data['warehouse'] as $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "warehouse_to_region SET warehouse_id = '" . (int)$warehouse_id . "', region = '" . (int)$value['region'] . "', cost = '" . (float) $value['cost'] . "'");
		}
	}

	public function deleteWarehouse($warehouse_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "warehouse` WHERE warehouse_id = '" . (int)$warehouse_id . "'");
	}

	public function getWarehouseGroup($warehouse_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "warehouse_group` fg LEFT JOIN " . DB_PREFIX . "warehouse_group_description fgd ON (fg.warehouse_id = fgd.warehouse_id) WHERE fg.warehouse_id = '" . (int)$warehouse_id . "' AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}


	public function getWarehouse($warehouse_id) {

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "warehouse_to_region WHERE warehouse_id = '" . (int)$warehouse_id . "'");

		return $query->row;
	}

	public function getWarehouses($data) {
		$sql = "SELECT *, (SELECT name FROM " . DB_PREFIX . "warehouse_group_description fgd WHERE f.warehouse_id = fgd.warehouse_id AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS `group` FROM " . DB_PREFIX . "warehouse f LEFT JOIN " . DB_PREFIX . "warehouse_description fd ON (f.warehouse_id = fd.warehouse_id) WHERE fd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['warehouse_name'])) {
			$sql .= " AND fd.name LIKE '" . $this->db->escape($data['warehouse_name']) . "%'";
		}

		$sql .= " ORDER BY f.sort_order ASC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getWarehouseCountries($warehouse_id) {
		$warehouse_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "warehouse_to_region WHERE warehouse_id = '" . (int)$warehouse_id . "'");

		return $warehouse_query->rows;
	}

	public function getTotalWarehouseGroups() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "warehouse_group`");

		return $query->row['total'];
	}
}
