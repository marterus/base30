<?php
// Heading
$_['heading_title']     = 'Warehouses';

// Text
$_['text_success']      = 'Success: You have modified warehouses!';
$_['text_list']         = 'Warehouse List';
$_['text_add']          = 'Add Warehouse';
$_['text_edit']         = 'Edit Warehouse';
$_['text_group']        = 'Warehouse Group';
$_['text_value']        = 'Warehouse Values';

// Column
$_['column_group']      = 'Warehouse Group';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_select_option']       = 'Select option for warehouse';
$_['entry_name']        = 'Warehouse Name';
$_['entry_sort_order']  = 'Sort Order';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify warehouses!';
$_['error_group']       = 'Warehouse Group Name must be between 1 and 64 characters!';
$_['error_name']        = 'Warehouse Name must be between 1 and 64 characters!';