<?php
// Heading
$_['heading_title']    = 'Warehouse';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Warehouse!';
$_['text_edit']        = 'Edit Warehouse';

// Entry
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';
$_['entry_cost'] = 'Cost';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Warehouse!';