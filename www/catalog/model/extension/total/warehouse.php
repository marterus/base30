<?php
class ModelExtensionTotalWarehouse extends Model {
	public function getTotal($total) {
		$this->load->language('extension/total/warehouse');

		$geoip_client = geoip_record_by_name($this->request->server['REMOTE_ADDR']);
		$warehouse = 0;
		if ($geoip_client) {
			$country_code3 = $geoip_client['country_code3'];
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE iso_code_3 = '" . $this->db->escape($country_code3) . "'");
			if ($query->num_rows) {
				$query_client = $this->db->query("SELECT * FROM " . DB_PREFIX . "warehouse_to_region WHERE region = '" . $this->db->escape($query->row['country_id']) . "'");
				$warehouse_client = [];
				foreach ($query_client->rows as $item) {
					$warehouse_client[(int) $item['warehouse_id']] = (float) $item['cost'];
				}
				if (!empty($warehouse_client)) {
					$products = $this->cart->getProducts();
					foreach ($products as $product) {
						if (isset($product['option'])) {
							foreach ($product['option'] as $option) {
								if (isset($warehouse_client[$option['option_value_id']])) {
									$warehouse += $warehouse_client[$option['option_value_id']];
								}
							}
						}

					}
				} else {
					$warehouse += (float) $this->config->get('total_warehouse_cost');
				}
			}
		}

		if ($warehouse) {
			$total['totals'][] = [
				'code'       => 'warehouse',
				'title'      => $this->language->get('text_warehouse'),
				'value'      => $warehouse,
				'sort_order' => $this->config->get('total_warehouse_sort_order'),
			];

			$total['total'] += $warehouse;
		}
	}

}